<?php
/**
* @project    Atom-M CMS
* @package    Chat Module
* @url        http://cms.modos189.ru
*/


class ChatModule extends Module {

    /**
     * @template  layout for module
     */
    public static $_template = 'chat';
    public $template = 'chat';

    /**
     * @module_title  title of module
     */
    public $module_title = 'Чат';

    /**
     * @module module indentifier
     */
    public static $_module = 'chat';
    public $module = 'chat';

    /**
     * default action ( show add form and iframe for messages )
     *
     * @return view content
     */
    public function index() {
        $content = '';
        if ($this->ACL->turn(array($this->module, 'add_materials'), false)) {
            $content = $this->form();
            $content .= $this->add_form();
        } else {
            $content = __('Permission denied');
        }
        
        // Navigation
        $nav = array();
        $nav['navigation'] = get_link(__('Home'), '/') . __('Separator') . __('chat');
        $this->_globalize($nav);
        
        return $this->_view($content);
    }
    
    public function ajax_messages($last = false) {
        $chatDataPath = ROOT . $this->getTmpPath('messages.dat');

        $messages = array();
        if (file_exists($chatDataPath)) {
            $data = unserialize(file_get_contents($chatDataPath));
            if (!empty($data)) {

                foreach ($data as $key => &$record) {
                    $record['message'] = $this->Textarier->print_page($record['message']);

                    // показывать ip и кнопку удаления для модераторов
                    if ($this->ACL->turn(array($this->module, 'delete_materials'), false)) {
                        $record['ip'] = '<a target="_blank" href="https://apps.db.ripe.net/search/query.html?searchtext=' . $record['ip'] . '" class="fps-ip" title="IP: ' . $record['ip'] . '"></a>';
                        $record['del'] = get_url($this->getModuleURL('del/'.$key));
                    } else {
                        $record['ip'] = '';
                        $record['del'] = '';
                    }
                    $record['date'] = date("Y-m-d", $record['unixtime']);
                    $record['time'] = date("h:i", $record['unixtime']);

                    // если передали unixtime последнего загруженного сообщения, отправлять только новые сообщения
                    if (!( ($last != false) and ($last >= $record['unixtime']) )) {
                        $messages[] = $data[$key];
                    }
                }
            }
        }

        echo json_encode($messages);
    }

    /**
     * add message form
     *
     * @return  none
     */
    public function add() {
        if (!$this->ACL->turn(array($this->module, 'add_materials'), false)) {
            return;
        }
        if (!isset($_POST['message'])) {
            die(__('Needed fields are empty'));
        }

        /* cut and trim values */
        $user_id = (!empty($_SESSION['user']['id'])) ? $_SESSION['user']['id'] : '0';
        $name = (!empty($_SESSION['user']['name'])) ? trim($_SESSION['user']['name']) : __('Guest');
        $message = trim(mb_substr($_POST['message'], 0, $this->Register['Config']->read('max_lenght', $this->module)));
        $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
        $keystring = (isset($_POST['captcha_keystring'])) ? trim($_POST['captcha_keystring']) : '';

        // Check fields
        $error = '';
        $valobj = $this->Register['Validate'];
        if (!empty($name) && !$valobj->cha_val($name, V_TITLE))
            $error = $error . '<li>' . __('Wrong chars in field "login"') . '</li>' . "\n";
        if (empty($message))
            $error = $error . '<li>' . __('Empty field "text"') . '</li>' . "\n";



        // Check captcha if need exists
        if (!$this->ACL->turn(array('other', 'no_captcha'), false)) {
            if (empty($keystring))
                $error = $error . '<li>' . __('Empty field "code"') . '</li>' . "\n";


            // Проверяем поле "код"
            if (!empty($keystring)) {
                // Проверяем поле "код" на недопустимые символы
                if (!$valobj->cha_val($keystring, V_CAPTCHA))
                    $error = $error . '<li>' . __('Wrong chars in field "code"') . '</li>' . "\n";
                if (!isset($_SESSION['chat_captcha_keystring'])) {
                    if (file_exists(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat')) {
                        $_SESSION['chat_captcha_keystring'] = file_get_contents(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
                        @_unlink(ROOT . '/sys/logs/captcha_keystring_' . session_id() . '-' . date("Y-m-d") . '.dat');
                    }
                }
                if (!isset($_SESSION['chat_captcha_keystring']) || $_SESSION['chat_captcha_keystring'] != $keystring)
                    $error = $error . '<li>' . __('Wrong protection code') . '</li>' . "\n";
            }
            unset($_SESSION['chat_captcha_keystring']);
        }

        /* if an errors */
        if (!empty($error)) {
            $_SESSION['addForm'] = array();
            $_SESSION['addForm']['error'] = '<p class="errorMsg">' . __('Some error in form') . '</p>' .
                    "\n" . '<ul class="errorMsg">' . "\n" . $error . '</ul>' . "\n";
            $_SESSION['addForm']['message'] = $message;
            die($_SESSION['addForm']['error']);
        }

        /* create dir for chat tmp file if not exists */
        $tmp = ROOT . $this->getTmpPath();
        if (!file_exists($tmp))
            mkdir($tmp, 0777, true);
        /* get data */
        if (file_exists($tmp . 'messages.dat')) {
            $data = unserialize(file_get_contents($tmp . 'messages.dat'));
        } else {
            $data = array();
        }


        /* cut data (no more 50 messages */
        while (count($data) > 50) {
            array_shift($data);
        }
        $data[] = array(
            'user_id' => $user_id,
            'name' => $name,
            'message' => $message,
            'ip' => $ip,
            'unixtime' => time(),
        );


        /* save messages */
        $file = fopen($tmp . 'messages.dat', 'w+');
        fwrite($file, serialize($data));
        fclose($file);
        die('ok');
    }

    /**
     * view add message form
     *
     * @return (str)  add form
     */
    public static function add_form() {
        $Register = Register::getInstance();


        $Parser = $Register['DocParser'];
        $Parser->templateDir = ChatModule::$_template;
        $ACL = $Register['ACL'];
        if (!$ACL->turn(array(ChatModule::$_module, 'add_materials'), false))
            return __('Dont have permission to write post');


        $markers = array();

        /* if an errors */
        if (isset($_SESSION['addForm'])) {
            $message = $_SESSION['addForm']['message'];
            unset($_SESSION['addForm']);
        } else {
            $message = '';
        }


        $kcaptcha = '';
        if (!$ACL->turn(array('other', 'no_captcha'), false)) {
            $kcaptcha = getCaptcha('chat_captcha_keystring');
        }
        $markers['action'] = get_url('/' . ChatModule::$_module . '/add/');
        $markers['message'] = h($message);
        $markers['captcha'] = $kcaptcha;


        $View = new Fps_Viewer_Manager();
        $View->setLayout(ChatModule::$_module);


        // TODO не нужно сюда копии меток пихать, нужно использовать глобальные метки как и везде
        $path = $Register['Config']->read('smiles_set');
        $path = (!empty($path) ? $path : 'atomm');
        $smiles_set = $path;
        $path = ROOT . '/data/img/smiles/' . $path . '/info.php';
        include $path;
        if (isset($smilesList) && is_array($smilesList)) {
            $smiles_list = (isset($smilesInfo) && isset($smilesInfo['show_count'])) ? array_slice($smilesList, 0, $smilesInfo['show_count']) : $smilesList;
        } else {
            $smiles_list = array();
        }


        $source = $View->view('addform.html', array('data' => $markers, 'template_path' => get_url('/template/' . getTemplateName()), 'smiles_set' => $smiles_set, 'smiles_list' => $smiles_list));

        return $source;
    }


    public function del($id = null) {
        $this->ACL->turn(array($this->module, 'delete_materials'));

        $id = intval($id);
        if ($id < 0) return;

        $chatDataPath = ROOT . $this->getTmpPath('messages.dat');
        if (file_exists($chatDataPath)) {
            $data = unserialize(file_get_contents($chatDataPath));
            if (!empty($data) and !empty($data[$id])) {
                unset($data[$id]);

                $file = fopen($chatDataPath, 'w');
                fwrite($file, serialize($data));
                fclose($file);
            }
        }
        echo true; 
    }


    /**
     * view messages form
     *
     * @return (str)  form
     */
    public static function form() {

        $View = new Fps_Viewer_Manager();
        $View->setLayout(ChatModule::$_module);

        $source = $View->view('list.html', array('template_path' => get_url('/template/' . getTemplateName())));

        return $source;
    }
}

