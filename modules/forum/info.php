<?php

$menuInfo = array(
    'url' => 'settings.php?m=forum',
    'ankor' => 'Форум',
	'sub' => array(
        'settings.php?m=forum' => 'Настройки',
        'design.php?m=forum' => 'Дизайн',
        'forum_cat.php' => 'Управление форумами',
        'forum_repair.php' => 'Пересчитать сообщения',
	),
);

$settingsInfo = array(
	'title' => array(
		'type' => 'text',
		'title' => 'Заголовок',
		'description' => 'Заголовок, который подставится в блок <title></title>',
	),
	'description' => array(
		'type' => 'text',
		'title' => 'Описание',
		'description' => 'То, что подставится в мета тег description',
	),
	'not_reg_user' => array(
		'type' => 'text',
		'title' => 'Псевдоним гостя',
		'description' => '(Под этим именем будет показано сообщение (пост) не зарегистрированного пользователя)',
	),
	'Ограничения' => 'Ограничения',
	'max_post_lenght' => array(
		'type' => 'text',
		'title' => 'Максимальная длина сообщения',
		'description' => '',
		'help' => 'Символов',
	),
	'posts_per_page' => array(
		'type' => 'text',
		'title' => 'Постов на странице',
		'description' => '',
		'help' => '',
	),
	'themes_per_page' => array(
		'type' => 'text',
		'title' => 'Тем на странице',
		'description' => '',
		'help' => '',
	),
	'Изображения' => 'Изображения',
	'use_preview' => array(
		'type' => 'checkbox',
		'title' => 'Использовать эскизы изображений',
		'description' => 'Возможность автоматического создания эскизов для больших изображений.',
		'value' => '1',
		'checked' => '1',
	),
	'img_size_x' => array(
		'type' => 'text',
		'title' => 'Ширина эскиза',
		'description' => 'Максимально допустимый размер эскиза по горизонтали.',
		'help' => 'px',
	),
	'img_size_y' => array(
		'type' => 'text',
		'title' => 'Высота эскиза',
		'description' => 'Максимально допустимый размер эскиза по вертикали.',
		'help' => 'px',
	),
    'Прикрепления' => 'Прикрепления',
    'locked_attaches' => array(
		'type' => 'checkbox',
		'title' => 'Запретить скачивание и прикрепление файлов',
		'description' => 'Вся система прикреплений модуля блокируется, но изображения, которые были прикреплены до блокировки, будут все еще видны в материале и в спец. метках для их вывода.',
		'value' => '1',
		'checked' => '1',
    ),
    'onlyimg_attaches' => array(
		'type' => 'checkbox',
		'title' => 'Разрешить загружать только изображения',
		'description' => 'Все, кроме изображений, будет блокироваться.',
		'value' => '1',
		'checked' => '1',
    ),
	'max_attaches' => array(
		'type' => 'text',
		'title' => 'Максимальное количество вложений',
		'description' => '',
		'help' => ''
	),
	'max_attaches_size' => array(
		'type' => 'text',
		'title' => 'Максимальный "вес" вложения',
		'description' => 'Определяет максимальный возможный размер прикрепления в килобайтах.',
		'help' => 'КБайт',
		'onview' => array(
			'division' => 1024,
		),
		'onsave' => array(
			'multiply' => 1024,
		)
    ),
    'Видео' => 'Видео',
	'video_size_x' => array(
		'type' => 'text',
		'title' => 'Ширина видео',
		'description' => 'Ширина устанавливаемого видео на сайт.',
		'help' => 'px',
	),
	'video_size_y' => array(
		'type' => 'text',
		'title' => 'Высота видео',
		'description' => 'Высота устанавливаемого видео на сайт.',
		'help' => 'px',
	),
	'Прочее' => 'Прочее',
	'active' => array(
		'type' => 'checkbox',
		'title' => 'Статус',
		'description' => '(Активирован/Деактивирован)',
		'value' => '1',
		'checked' => '1',
	),
);
