﻿<!doctype html>
<html>
    <head>
        <title>Atom-M CMS - вместе в будущее</title>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link type="text/css" rel="StyleSheet" href="css/style.css" />
        <script language="JavaScript" type="text/javascript" src="../data/js/jquery.js"></script>
    </head>
    <body>
        <div id="head"></div>
        <div id="container">
            <a id="begin" href="step1.php">НАЧАТЬ</a>
            <div id="descr">
                <div id="logo"></div>
                <h2>Внимание!</h2>
                Atom-M CMS предназначена, лишь, для личного использования и не должна распространяться от Вас
                третьим лицам. <br /><br />

                Копирование или распространение Atom-M CMS или ее частей без согласия автора (Brykin Andrey) 
                считается не законным. <br /><br />

                Вы в праве редактировать и изменять Вашу копию программы по своему усмотрению.<br /><br />

                Если данная копия программы досталась Вам не от официального источника (сайт <a href="http://atomx.net">http://atomx.net</a>), просим сообщить об этом нам (например на нашем форуме - <a href="http://atomx.net/forum/">http://atomx.net/forum</a>) и получить в замен лицензионную копию программы.<br /><br />

                Предупреждаем, что использование не лицензионных программ, может повлечь проблемы и является не безопасным.
            </div>
        </div>
        <div id="footer">
            <div style="float:left;">
                <a href="http://atomx.net/">OFF. SITE</a>
                <a href="http://home.develdo.com/">ABOUT</a>
                <a href="http://atomx.net/forum/">SUPPORT</a>
                <a href="https://bitbucket.org/atom-m/cms/wiki/Home">WIKI</a>
            </div>
            <div style="clear:both;"></div>
        </div>
    </body>
</html>