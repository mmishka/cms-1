<?php 
include_once '../sys/boot.php';
?><!doctype html>
<html>
<head>
<title>Atom-M CMS - вместе в будущее</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link type="text/css" rel="StyleSheet" href="css/style.css" />
<script language="JavaScript" type="text/javascript" src="../data/js/jquery.js"></script>
</head>
<body>
<div id="head">
	<ul id="progressbar"></ul>
</div>


<div id="container">

	<img src="img/ajax_loader.gif" style="display:none;" id="ajaxLoader" />
	<div class="finblock" id="queries"></div>

</div>

<div id="footer">
    <div style="float:left;">
        <a href="http://atomx.net/">OFF. SITE</a>
        <a href="http://home.develdo.com/">ABOUT</a>
        <a href="http://atomx.net/forum/">SUPPORT</a>
        <a href="https://bitbucket.org/atom-m/cms/wiki/Home">WIKI</a>
    </div>
    <div style="clear:both;"></div>
</div>
<script type="text/javascript">

function doQueries() {
	$('#ajaxLoader').show();
	$.get('do_queries.php?', function(data){
		$('#queries').html(data);
		$('#ajaxLoader').hide();
		progressBar(3);
		
		$('#queries').scrollTop(1500);
	});
}
function progressBar(step) {
	$('#ajaxLoader3').show();
	$.get('progressbar.php?step='+step, function(data){
		$('#ajaxLoader3').hide();
		$('#progressbar').html(data);
	});
}
progressBar(2);
doQueries();
</script>
</body>
</html>