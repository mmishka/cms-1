<?php
include_once '../sys/boot.php';

if (isset($_POST['send'])) {
	if (empty($_POST['host'])) $errors['db_host'] = 'Не заполненно поле "Хост базы данных"';
	if (empty($_POST['base'])) $errors['db_name'] = 'Не заполненно поле "Имя базы данных"';
	if (empty($_POST['user'])) $errors['db_user'] = 'Не заполненно поле "Логин"';
	
	if (empty($_POST['adm_login'])) $errors['adm_login'] = 'Не заполненно поле "Логин"';
	if (empty($_POST['adm_pass'])) $errors['adm_pass'] = 'Не заполненно поле "Пароль"';
	
	@$db = mysql_connect($_POST['host'], $_POST['user'], $_POST['pass']);
	if (!$db) $errors['connect'] = 'Не удалось подключиться к базе. Проверте настройки!';
	@$db = mysql_select_db($_POST['base'], $db);
	if (!$db) $errors['select'] = 'Не удалось найти базу. Проверте имя базы!';
	
	if (!empty($_POST['prefix']) && !preg_match('#^[a-z_]*$#i', $_POST['prefix'])) $errors['db_prefix'] = 'Не допустимые символы в поле "Префикс"';
	
	if (empty($errors)) {
		$settings = Config::read('all');
		$settings['db'] = array();
		$settings['db']['host']   = $_POST['host'];
		$settings['db']['name']   = $_POST['base'];
		$settings['db']['user']   = $_POST['user'];
		$settings['db']['pass']   = $_POST['pass'];
		$settings['db']['prefix'] = $_POST['prefix'];

		Config::write($settings);

		
		$_SESSION['adm_name'] = $_POST['adm_login'];
		$_SESSION['adm_pass'] = $_POST['adm_pass'];
		header ('Location: step2.php '); die();
	}	
}

?><!doctype html>
<html>
<head>
<title>Atom-M CMS - вместе в будущее</title>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link type="text/css" rel="StyleSheet" href="css/style.css" />
<script language="JavaScript" type="text/javascript" src="../data/js/jquery.js"></script>
</head>
<body>
<div id="head">
	<ul id="progressbar"></ul>
</div>


<div id="container">
	<div id="descr">
	<div id="newv"></div>
	
	<h3>Права на папки(с учетом вложенных файлов)</h3>
	<img src="img/ajax_loader.gif" style="display:none;" id="ajaxLoader" />
	<div id="checkAccess"></div>
	<br />
	<h3>Настройки сервера и PHP</h3>
	<img src="img/ajax_loader.gif" style="display:none;" id="ajaxLoader2" />
	<div id="checkServer"></div>
	<br />
	
	<h3>Настройки подключения к базе</h3>

	<form method="post" action="">
		<ul>
			<li>
				<span>Хост Сервера SQL</span>
				<span><div class="inp"><input id="sqlhostInp" type="text" name="host" value="localhost" /><span id="SQLserver">*</span></div></span>
			</li>
			<li>
				<span>Пользователь Базы</span>
				<span><div class="inp"><input id="sqluserInp" type="text" name="user" value="root" /><span id="SQLuser">*</span></div></span>
			</li>
			<li>
				<span>Пароль Пользователя</span>
				<span><div class="inp"><input id="sqlpassInp" type="text" name="pass" value="" /><span id="SQLpass">&nbsp;</span></div></span>
			</li>
            <li>
                <span>База Данных</span>
                <span><div class="inp"><input id="sqlbaseInp" type="text" name="base" value="fapos" /><span id="SQLbase">*</span></div></span>
            </li>
			<li>
				<span>Префикс таблиц</span>
				<span><div class="inp"><input id="sqlprefInp" type="text" name="prefix" value="" /><span id="SQLpref">&nbsp;</span></div></span>
			</li>
			<br/><br/>
			<li>
				<span>Логин Администратора</span>
				<span><div class="inp"><input type="text" name="adm_login" value="" /><span>*</span></div></span>
			</li>
			<li>
				<span>Пароль Администратора</span>
				<span><div class="inp"><input type="text" name="adm_pass" value="" /><span>*</span></div></span>
			</li>
		
		</table>
	</div>
		<input class="btn" type="submit" name="send" value="ПРОДОЛЖИТЬ" />
	</form> 
	<br />


</div>

<div id="footer">
    <div style="float:left;">
        <a href="http://atomx.net/">OFF. SITE</a>
        <a href="http://home.develdo.com/">ABOUT</a>
        <a href="http://atomx.net/forum/">SUPPORT</a>
        <a href="https://bitbucket.org/atom-m/cms/wiki/Home">WIKI</a>
    </div>
    <div style="clear:both;"></div>
</div>

<script type="text/javascript">

$(document).ready(function() {
    checkSQL();
    $('input[type=text]').keyup(function() {
        checkSQL();
        checkSQPrefix();
        return true;
    });
    $('input[type=text]').change(function() {
        checkSQL();
        checkSQPrefix();
        return true;
    });
});

function checkSQPrefix() {
    val = $('#sqlprefInp').val();
	if (/^[a-z_]*$/.test(val)) {
		$('#SQLpref').html('&nbsp;');
	} else {
		$('#SQLpref').html('<span style="color:#FF0000">Запрещенные символы</span>');
	}
}
function checkSQL() {
    base = $('#sqlbaseInp').val();
    host = $('#sqlhostInp').val();
    user = $('#sqluserInp').val();
    pass = $('#sqlpassInp').val();
    $.get('check_sql_server.php?host='+host+'&user='+user+'&pass='+pass, function(data){
        $('#SQLserver').html(data);
        $('#SQLuser').html(data);
        $('#SQLpass').html(data);
    });
    $.get('check_sql_server.php?base='+base+'&type=base&host='+host+'&user='+user+'&pass='+pass, function(data){
        $('#SQLbase').html(data);
    });
}
function checkAccess() {
	$('#ajaxLoader').show();
	$.get('check_access.php', function(data){
		$('#ajaxLoader').hide();
		$('#checkAccess').html(data);
	});
}
function checkServer() {
	$('#ajaxLoader2').show();
	$.get('check_server.php', function(data){
		$('#ajaxLoader2').hide();
		$('#checkServer').html(data);
	});
}
function progressBar() {
	$('#ajaxLoader3').show();
	$.get('progressbar.php', function(data){
		$('#ajaxLoader3').hide();
		$('#progressbar').html(data);
	});
}
function checkUpdate() {
	$.get('ping.php', function(data) {
		$('#newv').html(data);
	});
}
checkAccess();
checkServer();
progressBar();
checkUpdate();

<?php if (!empty($errors)): ?>
checkSQServer($('# id="sqlhostInp"').value, 'host');
checkSQServer($('# id="sqlusetInp"').value, 'user');
checkSQServer($('# id="sqlpassInp"').value, 'pass');
checkSQBase($('# id="sqlbaseInp"').value);
<?php endif; ?>
</script>
</body>
</html>