<?php
/**
* @project    Atom-M CMS
* @package    Editor templates for mail
* @url        http://cms.modos189.ru
*/


include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';
$pageTitle = 'Редактирование шаблона писем';
$Register = Register::getInstance();
$config = $Register['Config']->read('all');

$url_to_dir = ROOT . '/data/mail/' . $config['mail_template'];
$dir = opendir($url_to_dir);
$i = 1;
$good = false;
while ($file = readdir($dir)) { // проход по файлам в директории выбранного шаблона писем
    if (substr($file, -4) == '.msg') { // встречаем файл с расширением .msg
        $tmp[$i] = substr($file, 0, -4); // отрезаем расширение .msg у названия файла
        
        $url_to_file = $url_to_dir . '/' . $tmp[$i] . '.msg'; // формируем ссылку на файл
        
        if (isset($_POST['tmp_mail_' . $tmp[$i]])) {// если запрос существует
            $post = $_POST['tmp_mail_' . $tmp[$i]]; // считываем нужный POST запрос в переменную
            @file_put_contents($url_to_file, $post); // записываем в файл
            $good = true; // Информируем пользователя, что все хорошо
        }
        $tmp_code[$i] = @file_get_contents($url_to_file); // читаем содержимое файла
        
        $i++; // переходим к следующему файлу
    }
}
closedir($dir); 

$pageNav = $pageTitle;
$pageNavr = '';
include_once ROOT . '/admin/template/header.php';

if ($good) {
    echo '<div class="warning"><br /><b>Шаблон успешно отредактирован.</b><br /><br /></div>';
}
?>
<form method="POST" action="/admin/users_mail_tmp.php" enctype="multipart/form-data">
<div class="list">
	<div class="title"><?php echo $pageTitle . ' (' . $config['mail_template'] . ') '; ?></div>
	<div class="level1">
		<div class="head">
			<div class="title settings">Ключ</div>
			<div class="title-r">Значение</div>
			<div class="clear"></div>
		</div>
		<div class="items">
        <?php 
        for ($i = 1; $i <= count($tmp); $i++) {
        
                
              $output = '<div class="setting-item"><div class="left">';
             $output .= __('mail_tmp_' . $tmp[$i]); // название и комментарий ключа
             $output .= '</div>
                             <div class="right">';
             $output .= '<textarea wrap="off" name="tmp_mail_' . $tmp[$i] . '">' . $tmp_code[$i] . '</textarea>'; // поле редактирования шаблона
             $output .= '</div>
                                <div class="clear"></div>
                             </div>';
                
                
                echo $output;
        }
        ?>
            <div class="setting-item">
				<div class="left"></div>
				<div class="right">
					<input class="save-button" type="submit" value="Сохранить" />
				</div>
				<div class="clear"></div>
                <style>
                .right textarea {height:200px;width:505px}
                </style>
			</div>
		</div>
	</div>
</div>
</form>

<ul class="markers">
	<h2>Метки для использования в почтовых писмах</h2>
	<li><div class="global-marks">{{ fps_server_name }}</div> - Домен вашего сайта.</li>
	<li><div class="global-marks">{{ site_title }}</div> - Название вашего сайта.</li>
	<li><div class="global-marks">{{ mail.name }}</div> - Логин нового пользователя.</li>
	<li><div class="global-marks">{{ mail.password }}</div> - Пароль нового пользователя или новый пароль пользователя(восстановленный).</li>
	<li><div class="global-marks">{{ mail.link }}</div> - Ссылка(URL) на страницу активации нового аккаунта или восстановленного пароля.</li>
	<li><div class="global-marks">{{ fps_date }}</div> - Дата отправления письма.</li>
	<li><div class="global-marks">{{ fps_time }}</div> - Время отправления письма.</li>
	<li><div class="global-marks">{{ context }}</div> - Содержимое письма(то что вы вводите при массовой рассылке сообщений).</li>
	<li><div class="global-marks">{{ from.name }}</div> - Логин пользователя от которого пришло письмо.</li>
	<li><div class="global-marks">{{ mail.subject }}</div> - Сообщение отправленное вам на почту или в ЛС от пользователя {{ from.name }}.</li>
</ul>

<?php include_once 'template/footer.php'; ?>