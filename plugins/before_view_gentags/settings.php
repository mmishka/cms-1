<?php

$output    = '';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);
$template_patch = dirname(__FILE__).'/template/index.html';

if (isset($_POST['send'])) {
    $config['min_word'] = $_POST['min_word'];
    $config['min_repeat'] = $_POST['min_repeat'];
    $config['ignoring'] = $_POST['ignoring'];
    $config['ignoring_hide'] = $_POST['ignoring_hide'];
	
    file_put_contents($conf_pach, json_encode($config));
	
	$f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);

    $output .= '<div class="warning">Сохранено!<br><br></div>';
}

$template = file_get_contents($template_patch);

$output .= '<form action="" method="POST" enctype="multipart/form-data"><div class="list">';

$output .= '<div class="title">' . $config['title'] . ' - Настройки</div>';

$output .= '<div class="level1">
		<div class="items">
		    <div class="setting-item"><div class="title">Дефолтные настройки</div></div>
			<div class="setting-item">
			     <div class="left">Минимальная длинна тега: <span class="comment">по умолчанию</span></div>
			     <div class="right"><input type="text" name="min_word" value="' . $config['min_word'] . '"></div>
			     <div class="clear"></div>
			</div>
			<div class="setting-item">
			     <div class="left">Минимально количество совпадений: <span class="comment">по умолчанию</span></div>
			     <div class="right"><input type="text" name="min_repeat" value="' . $config['min_repeat'] . '"></div>
			     <div class="clear"></div>
			</div>
			<div class="setting-item">
			     <div class="left">Слова исключения: <span class="comment">по умолчанию</span></div>
			     <div class="right"><input type="text" name="ignoring" value="' . $config['ignoring'] . '"></div>
			     <div class="clear"></div>
			</div>
			<div class="setting-item"><div class="title">Зарезервированные настройки(Только админы)</div></div>
			<div class="setting-item">
			     <div class="left">Слова исключения зарезервированные: <span class="comment"></span></div>
			     <div class="right"><input type="text" name="ignoring_hide" value="' . $config['ignoring_hide'] . '"></div>
			     <div class="clear"></div>
			</div>
			<div class="setting-item"><div class="title">Содержимое метки {{ forms_gentags }}</div></div>
            <div class="setting-item">
                  <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? h($template) : '') . '</textarea>
            </div>
			<div class="setting-item">
				<div class="left"></div>
				<div class="right"><input class="save-button" type="submit" name="send" value="Сохранить"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>';
	
$output .= '</div></form>';

$output .= '<ul class="markers">
	<h2>Метки вызова генератора в шаблоне</h2>
	<li><div class="global-marks">{{ script_gentags }}</div> - Подключает скрипт с генератором тегов(должна быть подключена после jquery и до остальных меток и поля с текстом).</li>
	<li><div class="global-marks">{{ input_gentags }}</div> - Выводит идентификатор для поля, в который нужно вывести генерированные теги</li>
	<li><div class="global-marks">{{ button_gentags }}</div> - Выводит кнопку запуска генератора</li>
	<li><div class="global-marks">{{ forms_gentags }}</div> - Выводит дополнительные поля для настройки генератора(настраивается из админки)</li>
	<h2>Метки использующиеся в плагине для дополнительных форм настройки генератора{{ forms_gentags }}</h2>
	<li><div class="global-marks">{{ gentags.length_id }}</div> - Значение идентификатора для минимальной длинный слова</li>
	<li><div class="global-marks">{{ gentags.length_val }}</div> - Дефолтное значение value для минимальной длинный слова</li>
	<li><div class="global-marks">{{ gentags.repeat_id }}</div> - Значение идентификатора для минимального количества повторений слова</li>
	<li><div class="global-marks">{{ gentags.repeat_val }}</div> - Дефолтное значение value для минимального количества повторений слова</li>
	<li><div class="global-marks">{{ gentags.ignore_id }}</div> - Значение идентификатора для формы со словами-исключениями</li>
	<li><div class="global-marks">{{ gentags.ignore_val }}</div> - Дефолтное значение value для слов-исключений</li>
</ul>';

$output .= '<div class="list"><div class="title">Автор плагина <b>MrBoriska</b></div><div>';

?>