<?php
$obj_votes = new VotesSettings;
if (isset($_GET['plac']) && $_GET['plac'] === 'delete') {
    $obj_votes->delete();
} else if (isset($_GET['plac']) && $_GET['plac'] === 'add') {
    $obj_votes->add();
} else if (isset($_GET['plac']) && $_GET['plac'] === 'edit') {
    $obj_votes->edit();
}
$output = $obj_votes->pllist();







class VotesSettings {
    
    private $path;
    private $templ_path;
    
    
    
    
    public function __construct() {
        $this->path = dirname(__FILE__) . '/';
        $this->templ_path = dirname(__FILE__) . '/template/';
    }
    
    
    public function add() {
        if (empty($_GET['dir'])) redirect('admin/plugins.php');
        $dir = $_GET['dir'];
        
    
        $votes_pach = $this->path . 'votes_history.json';
        $history = (file_exists($votes_pach)) ? json_decode(file_get_contents($votes_pach), true) : array();
    
        $title = (!empty($_POST['title'])) ? $_POST['title'] : 'Unknown';
        $vars = (count($_POST['variant']) > 0) ? $_POST['variant'] : array();
        $fvars = array();
        foreach ($vars as $k => $variant) {
            $fvars[$variant] = 0;
        }
        unset($vars);
        if (empty($fvars)) $_SESSION['errors'] = '<li>Не заполнены варианты ответа</li>';
        
        
    
        $history[$title] = $fvars;
        file_put_contents($votes_pach, json_encode($history));
        
        
        redirect('admin/plugins.php?ac=edit&dir=' . $dir);
    }


    
    public function delete() {
        if (empty($_GET['dir'])) redirect('admin/plugins.php');
        $dir = $_GET['dir'];
        $id = (!empty($_GET['id'])) ? $_GET['id'] : 0;
        if (empty($id)) redirect('admin/plugins.php');
        
    
        $votes_pach = $this->path . 'votes_history.json';
        $history = (file_exists($votes_pach)) ? json_decode(file_get_contents($votes_pach), true) : array();
    
        if (!empty($history) && count($history) > 0) {
            foreach ($history as $key => $value) {
                if (md5($key) == $id) {
                    unset($history[$key]);
                    break;
                }
            }
        }
        file_put_contents($votes_pach, json_encode($history));
        redirect('admin/plugins.php?ac=edit&dir=' . $dir);
    }
    
    
    public function edit() {
        if (empty($_GET['dir']) || empty($_GET['id'])) redirect('admin/plugins.php');
        $history_file = R . 'plugins/' . $_GET['dir'] . '/votes_history.json';
        if (!file_exists($history_file)) redirect('admin/plugins.php');
        
        $votes = json_decode(file_get_contents($history_file), true);

        $errors = '';
        if (empty($_POST['title'])) $errors .= '<li>Заполните заголовок</li>';
        if (empty($_POST['variant']) || count($_POST['variant']) < 1) $errors .= '<li>Заполните варианты ответа</li>';
        if (!empty($errors)) {
            $_SESSION['form_errors'] = '<ul class="error">' . $errors . '</ul>';
            redirect('admin/plugins.php?ac=edit&dir=' . $_GET['dir']);
        }
        
        $title = $_POST['title'];
        $oldtitle = (!empty($_POST['oldval'])) ? html_entity_decode($_POST['oldval']) : '';
        $variants = array();
        foreach ($_POST['variant'] as $var) {
            if (!empty($votes[$oldtitle]) && !empty($votes[$oldtitle][$var])) {
                $variants[$var] = $votes[$oldtitle][$var];
                continue;
            }
            $variants[$var] = 0;
        }
        
        // Create array for saving
        $out = array();
        if (!empty($votes) && is_array($votes)) {
            if (!empty($votes[$oldtitle])) {
                unset($votes[$oldtitle]);
            }
        }
        $votes[$title] = $variants;
        file_put_contents($history_file, json_encode($votes));
        
        
        redirect('admin/plugins.php?ac=edit&dir=' . $_GET['dir']);
    }
    
    
    public function pllist() {
        $content = file_get_contents($this->templ_path . 'settings.html');
        $set_pach = $this->path . 'config.json';
        $votes_pach = $this->path . 'votes_history.json';

        $history = (file_exists($votes_pach)) ? json_decode(file_get_contents($votes_pach), true) : array();
        $set_fields = '';
        $dir = trim(strrchr(dirname(__FILE__), DS), DS);
        
        $out = '';
        $rows = '';
        $markers = array();
        $i = 0;
        if (!empty($history) && count($history) > 0) {
            foreach ($history as $key => $vote) {
                
                // colored ansvers
                $variants = array();
                $j = 0;
                foreach ($vote as $k => $v) {
                    $variants[$j]['id'] = $j+1;
                    $variants[$j]['name'] = $k;
                    $variants[$j]['color'] = $this->getColor($k);
                    $variants[$j]['cnt'] = $v;
                    $j++;
                }
                
                $markers['votes'][$i]['question'] = (!empty($key)) ? h($key) : 'Unknown';
                $markers['votes'][$i]['variables'] = array_slice($variants,0);
                $markers['votes'][$i]['voted'] = 'Всего голосов: ' . array_sum($vote);
                
                $markers['votes'][$i]['action']['edit_link'] = '<a class="edit" href="javascript:void(0)" onClick="openPopup(\'' . md5($key) . '_dWin\')"></a>';
                $markers['votes'][$i]['action']['del_link'] = '<a class="delete" onclick="return _confirm();" href="plugins.php?ac=edit&id=' . md5($key) . '&dir=' . h($dir) . '&plac=delete" title="Delete"></a>';
                
                $markers['votes'][$i]['action']['editform']['key'] = md5($key);
                
                $markers['votes'][$i]['action']['editform']['dir'] = $dir;
                
                $i++;
            }
        }
        
        
        if (empty($set_fields)) {
            //$markers['votes'] = array();
        }
        
        $markers['rows'] = $rows;
        $markers['settings'] = $set_fields;
        $markers['dir'] = $dir;
        $markers['uniqid'] = md5(rand());
        
        $Viewer = new Fps_Viewer_Manager;
        $content = $Viewer->parseTemplate($content, array('pl' => $markers));
        
        if (!empty($_SESSION['form_errors'])) {
            $content .= '<script type="text/javascript">showHelpWin(\'' . $_SESSION['form_errors'] . '\', \'Ошибки\');</script>';
            unset($_SESSION['form_errors']);
        }
        
        return $content;
    }
    
    private function getColor($k) {
        $color = substr(md5($k), 0, 6);
        $color = str_replace ('d','0',$color);
        $color = str_replace ('e','1',$color);
        $color = str_replace ('f','2',$color);
        return $color;
    }
}
?>