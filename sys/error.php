<?php
if (!empty($_GET['ac']) && is_numeric($_GET['ac'])) {
	$headers = array(
		'404' => "HTTP/1.0 404 Not Found",
		'403' => "HTTP/1.0 403 Forbidden You don't have permission to access / on this server.",
	);
	if (!empty($headers[$_GET['ac']])) 
        header($headers[$_GET['ac']]);
}
include_once 'boot.php';

if (!empty($_GET['ac']) && $_GET['ac'] == 'hack')
    $html = @file_get_contents(ROOT . '/data/errors/hack.html');
elseif (!empty($_GET['ac']) && $_GET['ac'] == '403')
    $html = @file_get_contents(ROOT . '/data/errors/403.html');
elseif (!empty($_GET['ac']) && $_GET['ac'] == '404')
    $html = @file_get_contents(ROOT . '/data/errors/404.html');
elseif (!empty($_GET['ac']) && $_GET['ac'] == 'ban')
    $html = @file_get_contents(ROOT . '/data/errors/ban.html');
else
    $html = @file_get_contents(ROOT . '/data/errors/default.html');

$Viewer = new Fps_Viewer_Manager();

$markers = array();
$markers['code'] = (empty($_GET['ac'])) ? '' : $_GET['ac'];
$markers['site_title'] = Config::read('site_title');
$markers['site_domain'] = $_SERVER['SERVER_NAME'];

echo $Viewer->parseTemplate($html, array('error' => $markers));

die();
?>