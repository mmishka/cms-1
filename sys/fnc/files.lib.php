<?php
/**
* @project    Atom-M CMS
* @package    Files library 
* @url        http://cms.modos189.ru
*/

/**
 * Check attached files
 *
 * @param string $module
 * @param bool $onlyimg (если разрешено загружать только изображения)
 */
 
function checkAttaches($module, $onlyimg = null) {
    $Register = Register::getInstance();
    $error = null;
    
    // Получаем максимальное количество допустимых прикреплений
	$max_attach = $Register['Config']->read('max_attaches', $module);
	if (empty($max_attach) || !is_numeric($max_attach)) $max_attach = 5;
    
    // Получаем максимально возможный размер файла
    $max_attach_size = $Register['Config']->read('max_attaches_size', $module);
	if (empty($max_attach_size) || !is_numeric($max_attach_size)) $max_attach_size = 1048576;
    
    // Если настройка не задана принудительно, то получаем её сами
    if (empty($onlyimg))
        $onlyimg = intval($Register['Config']->read('onlyimg_attaches', $module));
    
    for ($i = 1; $i <= $max_attach; $i++) {
            // Формируем имя формы с файлом
            $attach_name = 'attach' . $i;
            // Находим прикрепленный файл
            if (!empty($_FILES[$attach_name]['name'])) {
                $name = $_FILES[$attach_name]['name'];
                // Если точку не находим, то возвращаем запрет на такой файл.
                if (!strrpos($name, '.', 0))
                    $error .= '<li>' . __('Wrong file format') . '(' . $name . ')</li>' . "\n";
                // Проверяем файл на максимальный размер
                if ($_FILES[$attach_name]['size'] > $max_attach_size)
                    $error .= '<li>' . sprintf(__('Very big file'), $name, getSimpleFileSize($max_attach_size)) . '</li>' . "\n";
                // Если разрешено загружать только изображения проверяем и это
                if ($onlyimg and !isImageFile($_FILES[$attach_name]['type'], $name))
                    $error .= '<li>' . __('Wrong file format') . '(' . $name . ')</li>' . "\n";
            }
    }
    return $error;
}



 /**
 * Download attached files
 *
 * @param string $module
 * @param int $entity_id(id material or id post)
 * @param bool $unlink удалять ли прикрепления, если нужно
 */
function downloadAttaches($module, $entity_id, $unlink = false) {
	$Register = Register::getInstance();

	$error = null;
	if (empty($entity_id) || !is_numeric($entity_id)) return '<li>' . __('Some error occurred') . '</li>' . "\n";
	$files_dir = ROOT . '/data/files/' . $module . '/';
	// delete collizions if exists 
	//$this->deleteCollizions(array('id' => $post_id), true);
	
    // Получаем максимальное количество допустимых прикреплений
	$max_attach = $Register['Config']->read('max_attaches', $module);
	if (empty($max_attach) || !is_numeric($max_attach)) $max_attach = 5;
    
    // Получаем максимально возможный размер файла
    $max_attach_size = $Register['Config']->read('max_attaches_size', $module);
	if (empty($max_attach_size) || !is_numeric($max_attach_size)) $max_attach_size = 1048576;
            
	for ($i = 1; $i <= $max_attach; $i++) {
    
        // Формируем имя формы с файлом
		$attach_name = 'attach' . $i;
        
        // при редактировании, удаляет замененные файлы или у которых стоит глочка "удалить".
        if ($unlink and (!empty($_POST['unlink' . $i]) or !empty($_FILES[$attach_name]['name'])))
            deleteAttach($module, $entity_id, $i);
    
        // Находим прикрепленный файл
		if (!empty($_FILES[$attach_name]['name'])) {
            
            // Формируем имя файла
            $filename = getSecureFilename($_FILES[$attach_name]['name'], $entity_id, $i);
            
            // Узнаем, изображение ли мы загружаем
            $is_image = isImageFile($_FILES[$attach_name]['type'], $_FILES[$attach_name]['name']) ? 1 : 0;

            // Перемещаем файл из временной директории сервера в директорию files
            if (move_uploaded_file($_FILES[$attach_name]['tmp_name'], $files_dir . $filename)) {
                // Если изображение, накладываем ватермарк
                if ($is_image) {
                    $watermark_path = ROOT . '/data/img/' . ($Register['Config']->read('watermark_type') == '1' ? 'watermark_text.png' : $Register['Config']->read('watermark_img'));
                    if ($Register['Config']->read('use_watermarks') && !empty($watermark_path) && file_exists($watermark_path)) {
                        $waterObj = new FpsImg;
                        $save_path = $files_dir . $filename;
                        $waterObj->createWaterMark($save_path, $watermark_path);
                    }
                }
                
                // если возможно выставляем доступ к файлу.
                chmod($files_dir . $filename, 0644);
                
                // Формируем данные о файле
                $attach_file_data = array(
                    'user_id'       => $_SESSION['user']['id'],
                    'attach_number' => $i,
                    'filename'      => $filename,
                    'size'          => $_FILES[$attach_name]['size'],
                    'date'          => new Expr('NOW()')
                );
                
                if ($is_image)
                    $attach_file_data['is_image'] = $is_image;
                
                if ($module == 'forum')
                    $attach_file_data['post_id'] = $entity_id;
                else
                    $attach_file_data['entity_id'] = $entity_id;
                
                // Сохраняем данные о файле
                $className = ucfirst($module) . 'AttachesEntity';
                $entity = new $className($attach_file_data);
                if ($entity->save() == NULL)
                    $error .= '<li>' . sprintf(__('File is not load'), $_FILES[$attach_name]['name']) . '</li>' . "\n";
                
            } else 
                $error .= '<li>' . sprintf(__('File is not load'), $_FILES[$attach_name]['name']) . '</li>' . "\n";
		}
	}
	
	return $error;
}


 /**
 * Delete attached file
 *
 * @param string $module
 * @param int $entity_id(id material or id post)
 * @param int $attachNum номер прикрепленного файла
 */
function deleteAttach($module, $entity_id, $attachNum) {
    $Register = Register::getInstance();

	$attachModelClass = $Register['ModManager']->getModelNameFromModule($module . 'Attaches');
	$attachModel = new $attachModelClass;
    if ($module == 'forum')
        $attaches = $attachModel->getCollection(array(
            'post_id' => $entity_id,
            'attach_number' => $attachNum,
        ), array());
    else
        $attaches = $attachModel->getCollection(array(
            'entity_id' => $entity_id,
            'attach_number' => $attachNum,
        ), array());

    if (count($attaches) > 0 && is_array($attaches)) {
		foreach ($attaches as $attach) {
			if (!empty($attach)) {
				$filePath = ROOT . '/data/files/' . $module . '/' . $attach->getFilename();
				if (file_exists($filePath)) {
					_unlink($filePath);
				}
				$attach->delete();
			}
		}
    }
    return true;
}


/**
 * Create secure and allowed filename.
 * Check to dublicate;
 *
 * @param string $filename
 * @param string $dirToCheck - dirrectory to check by dublicate
 * @return string
 */
function getSecureFilename($filename, $post_id, $i) {
	
    // Извлекаем из имени файла расширение
    $ext = strrchr($filename, ".");
    
    // Если имя файла содержит не только литиницу и: '.', '-', '_' то "очищаем имя от примесей"
    if (preg_match("/(^[a-zA-Z0-9]+([a-zA-Z\_0-9\.-]*))$/" , $filename)==NULL) {
        $atmurl = new AtmUrl;
        $filename = $atmurl->translit($filename);
        $filename = str_replace(' ', '_', $filename);
    }
    
    
    // Формируем название файла
    if (!isPermittedFile($ext)) {
        //если расширение запрещенное, то заменяем его на .txt
        $filename = substr($filename, 0, strrpos($filename, '.', 0));
        $filename = ($filename) ? $filename : 'noname';
        $file = $post_id . '-' . $i . '_' . $filename . '.txt';
    } else
        $file = $post_id . '-' . $i . '_' . $filename;
    
    
	return $file;
}

function isImageFile($mime, $name) {
    // Извлекаем из имени файла расширение
    $ext = strrchr($name, ".");
    // Types of images
	$allowed_types = array('image/jpeg','image/jpg','image/gif','image/png', 'image/gif', 'image/pjpeg', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp');
	// Images extensions
	$img_extentions = array('.png','.jpg','.gif','.jpeg','.tiff', '.ico', '.bmp', '.wbmp');
	
	$is_image = in_array($mime, $allowed_types);
	if (!empty($ext)) $is_image = $is_image && in_array(strtolower($ext), $img_extentions);
	return $is_image;
}

function isPermittedFile($ext) {
	/**
	 * Wrong extention for download files
	 */
	$deny_extentions = array('.php', '.phtml', '.php3', '.php4', '.php5', '.html', '.htm', '.pl', '.js', '.htaccess', '.run', '.sh', '.bash', '.py');
	
	return !(empty($ext) || in_array(strtolower($ext), $deny_extentions));
}


function user_download_file($module, $file = null, $mimetype = 'application/octet-stream') {
    $Register = Register::getInstance();
    
    $error = null;
    //turn access
    $Register['ACL']->turn(array($module, 'download_files'));

    if (empty($file))
        return __('File not found');

    $path = ROOT . '/data/files/' . $module . '/' . $file;
    if (!file_exists($path))
        return __('File not found');
    $from = 0;
    $size = filesize($path);
    $to = $size;
    if (isset($_SERVER['HTTP_RANGE'])) {
        if (preg_match('#bytes=-([0-9]*)#', $_SERVER['HTTP_RANGE'], $range)) {// если указан отрезок от конца файла
            $from = $size - $range[1];
            $to = $size;
        } elseif (preg_match('#bytes=([0-9]*)-#', $_SERVER['HTTP_RANGE'], $range)) {// если указана только начальная метка
            $from = $range[1];
            $to = $size;
        } elseif (preg_match('#bytes=([0-9]*)-([0-9]*)#', $_SERVER['HTTP_RANGE'], $range)) {// если указан отрезок файла
            $from = $range[1];
            $to = $range[2];
        }
        header('HTTP/1.1 206 Partial Content');

        $cr = 'Content-Range: bytes ' . $from . '-' . $to . '/' . $size;
    } else
        header('HTTP/1.1 200 Ok');

    $etag = md5($path);
    $etag = substr($etag, 0, 8) . '-' . substr($etag, 8, 7) . '-' . substr($etag, 15, 8);
    header('ETag: "' . $etag . '"');
    header('Accept-Ranges: bytes');
    header('Content-Length: ' . ($to - $from));
    if (isset($cr))
        header($cr);
    header('Connection: close');
    header('Content-Type: ' . $mimetype);
    header('Last-Modified: ' . gmdate('r', filemtime($path)));
    header("Last-Modified: " . gmdate("D, d M Y H:i:s", filemtime($path)) . " GMT");
    header("Expires: " . gmdate("D, d M Y H:i:s", time() + 3600) . " GMT");
    $f = fopen($path, 'rb');


    if (preg_match('#^image/#', $mimetype))
        header('Content-Disposition: filename="' . substr($file, strpos($file, '_', 0)+1) . '";');
    else
        header('Content-Disposition: attachment; filename="' . substr($file, strpos($file, '_', 0)+1) . '";');

    fseek($f, $from, SEEK_SET);
    $size = $to;
    $downloaded = 0;
    while (!feof($f) and ($downloaded < $size)) {
        $block = min(1024 * 8, $size - $downloaded);
        echo fread($f, $block);
        $downloaded += $block;
        flush();
    }
    fclose($f);
}
