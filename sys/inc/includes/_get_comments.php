<?php
$id = (int)$entity->getId();
if (empty($id) || $id < 1) $html = true;

$commentsModel = $this->Register['ModManager']->getModelInstance('Comments');

if (empty($html) && $commentsModel) {
	
	$commentsModel->bindModel('author');

	/* pages nav */
	$where = array('entity_id' => $id, 'module' => $this->module);
    if (!$this->ACL->turn(array('other', 'can_see_hidden'), false))
        $where['premoder'] = 'confirmed';
	$total = $commentsModel->getTotal(array('cond' => $where));
	$per_page = intval(Config::read('comment_per_page', $this->module));
	if ($per_page < 1) $per_page = 10;
    list($pages, $page) = pagination($total, $per_page,  $this->getModuleURL('view/' . $id));
	$this->_globalize(array('comments_pagination' => $pages));


	$order_way = (Config::read('comments_order', $this->module)) ? 'DESC' : 'ASC';
	$params = array(
		'page'  => $page,
		'limit' => $per_page,
		'order' => 'date ' . $order_way,
	);
	$comments = $commentsModel->getCollection($where, $params);
	if ($comments) {
		foreach ($comments as $comment) {
			if ($comment) {
				$markers = array();


				// COMMENT ADMIN BAR
				$ip = ($comment->getIp()) ? $comment->getIp() : 'Unknown';
				$moder_panel = '';

                if (strtotime($comment->getEditdate()) > strtotime($comment->getDate()))
                    $lasttime = strtotime($comment->getEditdate());
                else
                    $lasttime = strtotime($comment->getDate());
                $raw_time_mess = $lasttime - time() + Config::read('raw_time_mess');
                if ($raw_time_mess <= 0) $raw_time_mess = false;

				if ($this->ACL->turn(array($this->module, 'edit_comments'), false)
                    || (!empty($_SESSION['user']['id']) && $comment->getUser_id() == $_SESSION['user']['id']
                    && $this->ACL->turn(array($this->module, 'edit_my_comments'), false)
                    && (Config::read('raw_time_mess') == 0 or $raw_time_mess))) {

					$moder_panel .= get_link('', $this->getModuleURL('/edit_comment_form/' . $comment->getId()), array('class' => 'fps-edit', 'title' => __('Edit')));
				}

				if ($this->ACL->turn(array($this->module, 'delete_comments'), false)
                    || (!empty($_SESSION['user']['id']) && $comment->getUser_id() == $_SESSION['user']['id']
                    && $this->ACL->turn(array($this->module, 'delete_my_comments'), false)
                    && (Config::read('raw_time_mess') == 0 or $raw_time_mess))) {

					$moder_panel .= get_link('', $this->getModuleURL('/delete_comment/' . $comment->getId()), array('class' => 'fps-delete', 'title' => __('Delete'), 'id' => 'fdc'.$comment->getId(), 'onClick' => "if (confirm('" . __('Are you sure') . "')) {sendu('fdc".$comment->getId()."')}; return false"));
				}
				
				$markers['raw_time_mess'] = $raw_time_mess;

				if (!empty($moder_panel)) {
					$moder_panel .= '<a target="_blank" href="https://apps.db.ripe.net/search/query.html?searchtext=' . h($ip) . '" class="fps-ip" title="IP: ' . h($ip) . '"></a>';
				}

				$markers['avatar'] = '<img class="ava" src="' . getAvatar($comment->getUser_id()) . '" alt="User avatar" title="' . h($comment->getName()) . '" />';
                $comments_author = $comment->getAuthor();
                $author_status =  ($comments_author && $comments_author->getStatus()) ? $comments_author->getStatus() : 0;
			    $status = $this->ACL->get_group_info();
                $status = $status[$author_status];
                $markers['user_group'] = h($status['title']);
				$markers['user_group_color'] = h($status['color']);

				if ($comment->getUser_id()) {
					$markers['name_a'] = get_link(h($comment->getName()), getProfileUrl((int)$comment->getUser_id()));
					$markers['user_url'] = get_url(getProfileUrl((int)$comment->getUser_id()));
					$markers['avatar'] = get_link($markers['avatar'], $markers['user_url']);
				} else {
					$markers['name_a'] = h($comment->getName());
				}
				$markers['name'] = h($comment->getName());


				$markers['moder_panel'] = $moder_panel;
				$markers['message'] = $this->Textarier->print_page($comment->getMessage());

				if ($comment->getEditdate()!='0000-00-00 00:00:00') {
					$markers['editdate'] = $comment->getEditdate();
				} else {
					$markers['editdate'] = '';
				}

				$comment->setAtom($markers);
			}
		}
	}
	$html = $this->render('viewcomment.html', array('commentsr' => $comments));


} else {
	$html = '';
}
