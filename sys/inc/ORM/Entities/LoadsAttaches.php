<?php
/**
* @project    Atom-M CMS
* @package    LoadsAttaches Entity
* @url        http://cms.modos189.ru
*/


/**
 *
 */
class LoadsAttachesEntity extends FpsEntity
{
	
	protected $id;
	protected $entity_id;
	protected $user_id;
	protected $attach_number;
	protected $filename ;
	protected $size;
	protected $date;
	protected $is_image;

	
	public function save()
	{
		$params = array(
			'entity_id' => intval($this->entity_id),
			'user_id' => intval($this->user_id),
			'attach_number' => intval($this->attach_number),
			'filename' => $this->filename,
			'size' => intval($this->size),
			'date' => $this->date,
			'is_image' => (!empty($this->is_image)) ? '1' : new Expr("'0'"),
		);
		if($this->id) $params['id'] = $this->id;
		$Register = Register::getInstance();
		return ($Register['DB']->save('loads_attaches', $params));
	}
	
	
	
	public function delete()
	{
		$path = ROOT . '/data/files/loads/' . $this->filename;
		if (file_exists($path)) unlink($path);

        if (Config::read('use_local_preview', $this->module)) {
            $preview = Config::read('use_preview', $this->module);
            $size_x = Config::read('img_size_x', $this->module);
            $size_y = Config::read('img_size_y', $this->module);
        } else {
            $preview = Config::read('use_preview');
            $size_x = Config::read('img_size_x');
            $size_y = Config::read('img_size_y');
        }
        $path = ROOT.'/sys/tmp/img_cache/'.$size_x.'x'.$size_y.'/loads/'.$this->filename;
        unlink($path);

		$Register = Register::getInstance();
		$Register['DB']->delete('loads_attaches', array('id' => $this->id));
	}
}