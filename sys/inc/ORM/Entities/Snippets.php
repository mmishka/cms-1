<?php
/**
* @project    Atom-M CMS
* @package    Snippets Entity
* @url        http://cms.modos189.ru
*/


/**
 *
 */
class SnippetsEntity extends FpsEntity
{
	
	protected $id;
	protected $name;
	protected $body;
	
	
	public function save()
	{
		$params = array(
			'name' => $this->ips,
			'body' => $this->cookie,
		);
		
		if ($this->id) $params['id'] = $this->id;
		$Register = Register::getInstance();
		return $Register['DB']->save('snippets', $params);
	}
	
	
	
	public function delete()
	{ 
		$Register = Register::getInstance();
		$Register['DB']->delete('snippets', array('id' => $this->id));
	}


}