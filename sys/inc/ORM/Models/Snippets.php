<?php
/**
* @project    Atom-M CMS
* @package    Snippets Model
* @url        http://cms.modos189.ru
*/


/**
 *
 */
class SnippetsModel extends FpsModel
{
	public $Table = 'snippets';

    protected $RelatedEntities = array(
    );

	public function getByName($name)
	{
        $Register = Register::getInstance();
		$entity = $this->getDbDriver()->select($this->Table, DB_FIRST, array(
			'cond' => array(
				'name' => $name
			)
		));

		if (!empty($entity[0])) {
			$entityClassName = $Register['ModManager']->getEntityNameFromModel(get_class($this));
			$entity = new $entityClassName($entity[0]);
			return (!empty($entity)) ? $entity : false;
		}
		return false;
	}
}