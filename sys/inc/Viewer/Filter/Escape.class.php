<?php
/**
* @project    Atom-M CMS
* @package    Escape filter
* @url        http://cms.modos189.ru
*/

class Fps_Viewer_Filter_Escape {


	public function compile($value)
	{
		return "htmlspecialchars($value)";
	}
}